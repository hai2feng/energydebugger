package plugin.utility;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import nus.edu.sg.Global;
import nus.edu.sg.Log;
import nus.edu.sg.ds.ClassNode;
import nus.edu.sg.ds.LoopNode;
import nus.edu.sg.ds.LoopNode.LoopType;
import nus.edu.sg.ds.MethodNode;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IMember;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IPackageDeclaration;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.ITypeHierarchy;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.CatchClause;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.DoStatement;
import org.eclipse.jdt.core.dom.ForStatement;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.VariableDeclaration;
import org.eclipse.jdt.core.dom.WhileStatement;
import org.eclipse.jdt.internal.corext.callhierarchy.CallHierarchy;
import org.eclipse.jdt.internal.corext.callhierarchy.MethodWrapper;

/**
 * The Class Parser.
 */
public class Parser {

	private static String cn = "Parser";

	/**
	 * Gets the list of projects.
	 * 
	 * @return the list of projects
	 * @throws JavaModelException
	 *             the java model exception
	 * @throws CoreException
	 *             the core exception
	 */
	public static List<String> getListOfProjects() throws JavaModelException, CoreException {

		final List<String> temp = new ArrayList<String>();
		final IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();

		final IProject[] projects = root.getProjects();
		if (projects.length == 0) {
			Log.e("No projects found in workspace");
			temp.add("empty");
			return temp;
		}

		// TODO: must add some mechanism to detect whether the projects are
		// Android projects or not
		// For instance check if it contains the AndroidManifest.xml, etc
		for (final IProject project : projects) {
			temp.add(project.getName());
			// System.out.println("Project " + project.getName() + " path " +
			// project.getLocation().toOSString());
			Global.pathToProjects.put(project.getName(), project.getLocation().toOSString());

		}

		return temp;
	}

	/**
	 * Process project.
	 * 
	 * @throws JavaModelException
	 *             the java model exception
	 * @throws CoreException
	 *             the core exception
	 */
	public static void processProject() throws JavaModelException, CoreException {
		Log.i("[" + cn + "] Started parsing project ");
		final IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();

		final IProject project = root.getProject(Global.selectedProject);

		if (project != null) {

			if (!project.isOpen()) {
				System.out.println("Project is not open. Please open project and retry.");
				return;
			}
			parseSourceFiles(project);
			Global.parsingComplete = true;

			// for (final String k : Global.sourceToLineMap.keySet()) {
			// System.out.println(k + ":" +
			// Global.sourceToLineMap.get(k).isService() + ":"
			// + Global.sourceToLineMap.get(k).isActivity());
			// }
			//
			// System.out.println("=========stop and test===========");

		} else {
			Log.e("Project " + Global.selectedProject + " not found");
		}
	}

	/**
	 * Parses the source files.
	 * 
	 * @param project
	 *            the project
	 * @throws JavaModelException
	 *             the java model exception
	 * @throws CoreException
	 *             the core exception
	 */
	private static void parseSourceFiles(IProject project) throws JavaModelException, CoreException {
		if (project.isNatureEnabled("org.eclipse.jdt.core.javanature")) {

			Global.iMethodMap = new HashMap<String, IMethod>();
			final IJavaProject javaProject = JavaCore.create(project);
			final IPackageFragment[] packages = javaProject.getPackageFragments();

			for (final IPackageFragment aPackage : packages) {

				if (aPackage.getKind() == IPackageFragmentRoot.K_SOURCE) {

					for (final ICompilationUnit iCompilationUnit : aPackage.getCompilationUnits()) {

						final IPackageDeclaration[] packList = iCompilationUnit.getPackageDeclarations();

						final IResource resource = iCompilationUnit.getUnderlyingResource();
						IFile ifile = null;
						if (resource.getType() == IResource.FILE) {

							ifile = (IFile) resource;
							Global.TEST_IFILE = ifile;
						}

						if (Global.TEST_IFILE == null) {
							Global.TEST_IFILE = ifile;
						}

						boolean isActivity = false;
						boolean isService = false;
						boolean isListener = false;

						final IType[] types = iCompilationUnit.getAllTypes();
						for (final IType type : types) {
							final ITypeHierarchy th = type.newTypeHierarchy(null);

							for (final IType t : th.getAllTypes()) {

								// System.out.println(t.getKey());
								if (t.getKey().contains("Landroid/app/Activity;")
										|| t.getKey().contains("Landroid/maps/MapActivity")
										|| t.getKey().contains("Lde/tui/itlogger/iTloggerActivity")) {
									// System.out.println(" activity " +
									// iCompilationUnit.getElementName());
									isActivity = true;
								}

								if (t.getKey().contains("Landroid/app/Service;")) {
									// System.out.println(" service " +
									// iCompilationUnit.getElementName());
									isService = true;
								}

								if (t.getKey().contains("Landroid/location/LocationListener;")
										|| t.getKey().contains("Landroid/hardware/SensorEventListener")) {
									// System.out.println(" activity " +
									// iCompilationUnit.getElementName());
									isListener = true;
								}
							}

							// System.out.println(th);
						}

						// /////////////////
						final IType[] allTypes = iCompilationUnit.getAllTypes();
						for (final IType type : allTypes) {

							final IMethod[] methods = type.getMethods();

							for (final IMethod method : methods) {
								String mKey = method.getPath().toString().replace("/", ".").replace(".java", "")
										+ ";->" + method.getElementName() + method.getSignature();

								int ids = mKey.indexOf(".src.");
								if (ids != -1) {
									mKey = mKey.substring(ids + 5);
								}

								ids = mKey.indexOf("(");
								if (ids != -1) {
									mKey = mKey.substring(0, ids);
								}
								// System.out.println(mKey);
								// System.out.println("Called by : " +
								// getCallersOf(method));
								Global.iMethodMap.put(mKey, method);
							}
						}

						// /////////////

						final String className = iCompilationUnit.getElementName();
						String packageName = "";
						if (packList.length > 0) {
							packageName = packList[0].getElementName();
						}

						final ClassNode cNode = new ClassNode(iCompilationUnit.getPath().toString(), className,
								packageName, ifile);
						if (isActivity) {
							cNode.setActivity();
						}

						if (isService) {
							cNode.setService();
						}

						if (isListener) {
							cNode.setListener();
						}

						final String pathToFile = Global.pathToSelectedProject.replace("\\", "/")
								+ iCompilationUnit.getPath().toString().substring(1 + Global.selectedProject.length());
						// System.out.println("parsing source file :" +
						// pathToFile);

						final ASTParser parser = ASTParser.newParser(AST.JLS3);
						parser.setKind(ASTParser.K_COMPILATION_UNIT);
						parser.setSource(iCompilationUnit);
						parser.setResolveBindings(true);

						final CompilationUnit cunit = (CompilationUnit) parser.createAST(null);

						cunit.accept(new ASTVisitor() {

							@Override
							public boolean visit(DoStatement node) {

								final Statement statement = node.getBody();
								final int lineNumber = cunit.getLineNumber(node.getStartPosition()) - 1;
								final LoopNode lnode = new LoopNode(LoopType.DO, statement.toString(), lineNumber,
										getEndLineNumber(lineNumber, statement.toString(), pathToFile));
								cNode.addLoops(lnode);
								return true;

							}

							@Override
							public boolean visit(ForStatement node) {

								final Statement statement = node.getBody();
								final int lineNumber = cunit.getLineNumber(node.getStartPosition()) - 1;
								final LoopNode lnode = new LoopNode(LoopType.FOR, statement.toString(), lineNumber,
										getEndLineNumber(lineNumber, statement.toString(), pathToFile));
								cNode.addLoops(lnode);
								return true;

							}

							@Override
							public boolean visit(WhileStatement node) {

								final Statement statement = node.getBody();
								final int lineNumber = cunit.getLineNumber(node.getStartPosition()) - 1;

								final LoopNode lnode = new LoopNode(LoopType.WHILE, statement.toString(), lineNumber,
										getEndLineNumber(lineNumber, statement.toString(), pathToFile));

								// System.out.println(" While loop " +
								// lnode.getStartLine() + " -- " +
								// lnode.getEndLine());

								cNode.addLoops(lnode);
								return true;

							}

							@Override
							public boolean visit(CatchClause node) {

								final Statement doStBlock = node.getBody();
								final int lineNumber = cunit.getLineNumber(node.getStartPosition()) - 1;
								// System.out.println("Catch clause start : " +
								// doStBlock + " line number " + lineNumber);
								return true;

							}

							@Override
							public boolean visit(final MethodDeclaration node) {

								final Block methodBlock = node.getBody();

								List<MethodNode> intenalMthds = null;
								String myblock = null;
								if (methodBlock != null) {
									myblock = methodBlock.toString();
									intenalMthds = methodVisitor(myblock);
								}

								final List<String> parameters = new ArrayList<String>();
								for (final Object parameter : node.parameters()) {
									final VariableDeclaration variableDeclaration = (VariableDeclaration) parameter;
									String type = variableDeclaration.getStructuralProperty(
											SingleVariableDeclaration.TYPE_PROPERTY).toString();
									for (int i = 0; i < variableDeclaration.getExtraDimensions(); i++) {
										type += "[]";
									}
									final IVariableBinding ivb = variableDeclaration.resolveBinding();

									if (ivb == null) {

										parameters.add(type);

									} else {
										final String fqn = ivb.getType().getQualifiedName();

										parameters.add(fqn);
									}

								}

								String returnType = "";
								if (node.getReturnType2() != null) {
									returnType = node.getReturnType2().toString();
								}

								final int lineNumber = cunit.getLineNumber(node.getStartPosition()) - 1;
								final int endLineNumber = getEndLineNumber(lineNumber, myblock, pathToFile);

								final MethodNode mNode = new MethodNode(node.getName().toString(), parameters,
										returnType, lineNumber, endLineNumber, intenalMthds);
								// System.out.println("Method : " +
								// node.getName().toString());
								cNode.addMethod(mNode);

								return true;
							}

							private int getEndLineNumber(int lineNumber, String myblock, String pathToFile) {
								if (myblock == null) {
									return lineNumber + 1;
								}
								final String[] lines = myblock.split("\r\n|\r|\n");
								final int backupEndLine = lines.length + lineNumber;

								// some fancy calculation
								return doFancyCalculation(lineNumber, backupEndLine, myblock, pathToFile);
							}

							private int doFancyCalculation(int lineNumber, int backupEndLine, String myblock,
									String pathToFile) {

								final ArrayList<String> fileAsList = new ArrayList<String>();

								try {

									final FileReader fileReader = new FileReader(pathToFile);
									final BufferedReader bufferedReader = new BufferedReader(fileReader);
									String line = null;
									while ((line = bufferedReader.readLine()) != null) {
										fileAsList.add(line);
									}
									bufferedReader.close();

								} catch (final IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
									return backupEndLine;
								}

								// get the first line
								final String[] lines = myblock.split("\r\n|\r|\n");
								int jumped = 0;
								String lastLine = null;
								String firstLine = null;
								int i = 0;
								for (i = lines.length - 1; i > 0; i--) {
									final String temp = lines[i].replace("}", "").replace("(", "").trim();

									if (temp.length() <= 0) {
										jumped++;
										continue;
									}
									lastLine = lines[i];

									break;
								}

								int j = 0;
								for (j = 0; j < lines.length; j++) {
									final String temp = lines[j].replace("{", "").replace("(", "").trim();

									if (temp.length() <= 0) {
										continue;
									}
									firstLine = lines[j];

									break;
								}

								if (lastLine == null || firstLine == null) {
									return backupEndLine;
								} else if (lastLine.contains(firstLine)) {
									return backupEndLine;
								}

								final int lastIdx = getIndex(fileAsList, lastLine);

								if (lastLine != null && count(lastLine, myblock) == 1) {
									// System.out.println(lastLine +
									// " linenumber " + (lastIdx + jumped)
									// + " as compared to backup " +
									// backupEndLine);

									if (lastIdx + jumped > backupEndLine) {
										backupEndLine = lastIdx + jumped;
									}

								}
								return backupEndLine;
							}

							private int getIndex(ArrayList<String> fileAsList, String line) {

								int idx = 0;

								int count = 0;
								for (final String l : fileAsList) {
									if (idx == 0) {
										count++;
									}
									// System.out.println(l);
									if (l.replace(" ", "").contains(line.replace(" ", ""))) {
										idx++;
									}

								}

								if (idx == 0 || idx > 1) {
									return -1;
								}

								return count;
							}

							private int count(String toCheck, String myblock) {
								final int ret = myblock.length() - myblock.replace(toCheck, "").length();
								final int size = toCheck.length();

								// System.out.println(" to check " + toCheck +
								// " count " + ret / size);
								return ret / size;
							}

							private List<MethodNode> methodVisitor(String content) {

								final List<MethodNode> ret = new ArrayList<MethodNode>();

								final ASTParser metparse = ASTParser.newParser(AST.JLS3);
								metparse.setSource(content.toCharArray());
								metparse.setKind(ASTParser.K_STATEMENTS);

								final Block block = (Block) metparse.createAST(null);

								block.accept(new ASTVisitor() {

									@Override
									public boolean visit(MethodInvocation node) {
										final int lineNumber = cunit.getLineNumber(node.getStartPosition()) - 1;
										;
										final MethodNode internalMethod = new MethodNode(node.getName()
												.getFullyQualifiedName(), node.arguments(), "", lineNumber,
												lineNumber + 1, null);
										ret.add(internalMethod);
										// System.out.println("\tinternal method: "
										// +
										// node.getName().getFullyQualifiedName()
										// + " at " + lineNumber);

										return false;
									}

								});

								return ret;

							}

						});

						Global.sourceToLineMap.put(packageName + "." + className.replace(".java", ""), cNode);

					}

				}
			}
		}

	}

	public static HashSet<IMethod> getCallersOf(IMethod m) {

		final CallHierarchy callHierarchy = CallHierarchy.getDefault();

		final IMember[] members = { m };

		final MethodWrapper[] methodWrappers = callHierarchy.getCallerRoots(members);
		final HashSet<IMethod> callers = new HashSet<IMethod>();
		for (final MethodWrapper mw : methodWrappers) {
			final MethodWrapper[] mw2 = mw.getCalls(new NullProgressMonitor());
			final HashSet<IMethod> temp = getIMethods(mw2);
			callers.addAll(temp);
		}

		return callers;
	}

	public static HashSet<IMethod> getIMethods(MethodWrapper[] methodWrappers) {
		final HashSet<IMethod> c = new HashSet<IMethod>();
		for (final MethodWrapper m : methodWrappers) {
			final IMethod im = getIMethodFromMethodWrapper(m);
			if (im != null) {
				c.add(im);
			}
		}
		return c;
	}

	public static IMethod getIMethodFromMethodWrapper(MethodWrapper m) {
		try {
			final IMember im = m.getMember();
			if (im.getElementType() == IJavaElement.METHOD) {
				return (IMethod) m.getMember();
			}
		} catch (final Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}