package plugin.views;

import java.util.List;

import nus.edu.sg.ds.Tupple;

import org.eclipse.jdt.internal.ui.JavaPlugin;
import org.eclipse.jdt.internal.ui.javaeditor.CompilationUnitEditor;
import org.eclipse.jdt.ui.text.IJavaPartitions;
import org.eclipse.jdt.ui.text.JavaSourceViewerConfiguration;
import org.eclipse.jdt.ui.text.JavaTextTools;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.swt.custom.LineBackgroundEvent;
import org.eclipse.swt.custom.LineBackgroundListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Composite;

public class CodeEditor extends CompilationUnitEditor {

	public static String ID = "plugin.views.CodeEditor";
	// public static int startHighlight;
	// public static int endHighlight;

	public static List<Tupple> toHighlight;

	JavaTextTools javaTextTools;

	public CodeEditor() {
		super();

		javaTextTools = new JavaTextTools(JavaPlugin.getDefault().getCombinedPreferenceStore());

		final JavaSourceViewerConfiguration sourceViewerConfiguration = new JavaSourceViewerConfiguration(
				javaTextTools.getColorManager(), JavaPlugin.getDefault().getCombinedPreferenceStore(), this,
				IJavaPartitions.JAVA_PARTITIONING);

	}

	@Override
	public void createPartControl(Composite composite) {
		super.createPartControl(composite);

		getSourceViewer().resetVisibleRegion();

		final Color color = new Color(composite.getDisplay(), new RGB(255, 255, 0));

		final LineBackgroundListener lineBackgroundListener = new LineBackgroundListener() {

			@Override
			public void lineGetBackground(LineBackgroundEvent event) {
				final int offset = event.lineOffset;

				final IDocument document = getDocumentProvider().getDocument(getEditorInput());
				try {
					final int line = document.getLineOfOffset(offset);

					document.getPartition(offset);
					// System.out.println(line);
					if (isHighlighted(line)) {
						event.lineBackground = color;
					}
				} catch (final BadLocationException e) {
					// TODO Auto-generated catch block
					System.out.println("exception");
					e.printStackTrace();
				}

				// System.out.println("offset " + offset);
				// System.out.println(" content " + event.lineText);
				// System.out.println(" time " + event.time);

			}

			private boolean isHighlighted(int line) {

				for (final Tupple t : toHighlight) {
					if (line >= t.start && line <= t.end) {
						return true;
					}
				}

				return false;
			}
		};
		// Install line background color listener on source viewer
		getSourceViewer().getTextWidget().addLineBackgroundListener(lineBackgroundListener);

	}
}
