package plugin.views;

import org.eclipse.jface.text.rules.IWordDetector;

public class MyWordDetector implements IWordDetector {

	@Override
	public boolean isWordStart(char c) {
		return Character.isJavaIdentifierPart(c);
	}

	@Override
	public boolean isWordPart(char c) {
		return Character.isJavaIdentifierStart(c);
	}

}
