package plugin.views;

import java.util.HashMap;

import nus.edu.sg.ds.DNode;
import nus.edu.sg.ds.Defect;
import nus.edu.sg.ds.EnergyAux;

import org.eclipse.draw2d.FigureCanvas;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.zest.core.widgets.Graph;
import org.eclipse.zest.core.widgets.GraphConnection;
import org.eclipse.zest.core.widgets.GraphNode;
import org.eclipse.zest.core.widgets.ZestStyles;
import org.eclipse.zest.layouts.LayoutStyles;
import org.eclipse.zest.layouts.algorithms.TreeLayoutAlgorithm;
import org.eclipse.zest.layouts.algorithms.VerticalLayoutAlgorithm;

public class ImageView extends ViewPart {

	enum nodeName {
		activity_start, onCreate_called, onStart_called, onResume_called, activity_running, pause_action, onPause_called, onStop_called, onDestroy_called, activity_shutdown
	};

	public static final String ID = "plugin.views.imageview";

	private static Graph graph;
	private static Color color_Red;
	private static Color color_Green;
	private static Color color_Blue;
	private static Color color_taint;
	private static Color color_gray;

	private static String lastName = null;
	private static HashMap<Object, GraphNode> gNodeMap;

	private int size_x = 1024;
	private int size_y = 760;

	private static Composite localParent;

	@Override
	public void createPartControl(Composite parent) {
		// System.out.println("called image view, " +
		// parent.getShell().getSize());

		size_x = parent.getShell().getSize().x;
		size_y = parent.getShell().getSize().y;
		System.out.println(" size " + size_x);

		final GridLayout gridlayout = new GridLayout(1, false);
		parent.setLayout(gridlayout);

		localParent = parent;
		graph = new Graph(localParent, SWT.NONE);
		graph.setLayoutData(new GridData(GridData.FILL_BOTH));
		graph.setScrollBarVisibility(FigureCanvas.AUTOMATIC);

		color_Red = parent.getDisplay().getSystemColor(SWT.COLOR_RED);
		color_Green = parent.getDisplay().getSystemColor(SWT.COLOR_GREEN);
		color_Blue = parent.getDisplay().getSystemColor(SWT.COLOR_CYAN);
		color_taint = parent.getDisplay().getSystemColor(SWT.COLOR_YELLOW);
		color_gray = parent.getDisplay().getSystemColor(SWT.COLOR_GRAY);

		gNodeMap = new HashMap<>();

		addNode("App_Start", null, color_gray);
		lastName = "App_Start";

		final VerticalLayoutAlgorithm vl = new VerticalLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING);
		vl.setRowPadding(2);
		graph.setLayoutAlgorithm(vl, true);

	}

	private static void addNode(String name, String lastName, Color color) {

		if (gNodeMap.containsKey(name)) {
			return;
		}

		final GraphNode gNode = new GraphNode(graph, SWT.NONE, name);
		if (color == null) {
			gNode.setBackgroundColor(color_Blue);
		} else {
			gNode.setBackgroundColor(color);
		}
		gNodeMap.put(name, gNode);

		if (lastName != null) {
			final GraphConnection gcon = new GraphConnection(graph, ZestStyles.CONNECTIONS_DIRECTED,
					gNodeMap.get(lastName), gNodeMap.get(name));
			gcon.setWeight(0.5);
			gcon.setLineWidth(1);
		}

	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub

	}

	public static void populate(Defect defect) {
		String key;
		for (final DNode b : defect.before) {
			if (b.getClassName().equals("Activity")) {
				continue;
			}
			key = b.getClassName() + "_" + b.getMethodName();
			addNode(key, lastName, color_Blue);
			lastName = key;
		}

		key = getKey(defect.caller);

		if (!gNodeMap.containsKey(key)) {
			addNode(key, lastName, color_Red);
			lastName = key;
			gNodeMap.get(key).setBackgroundColor(color_Red);
		} else {
			gNodeMap.get(key).setBackgroundColor(color_Red);
		}

		String resource = defect.getMethod();
		final int idx = resource.indexOf("(");
		if (idx != -1) {
			resource = resource.substring(0, idx);
		}

		final GraphNode gNode = new GraphNode(graph, SWT.NONE, resource);
		gNode.setBackgroundColor(color_taint);

		final GraphConnection gcon = new GraphConnection(graph, ZestStyles.CONNECTIONS_DIRECTED,
				gNodeMap.get(lastName), gNode);
		gcon.setWeight(0.5);
		gcon.setLineWidth(1);

		for (final DNode a : defect.after) {
			if (a.getClassName().equals("Activity")) {
				continue;
			}
			key = a.getClassName() + "_" + a.getMethodName();
			addNode(key, lastName, color_Green);
			lastName = key;
		}

		// highlight the environment
		for (final EnergyAux env : defect.envn) {
			final String hKey = getKey(env.getCaller());
			if (gNodeMap.containsKey(hKey)) {
				// gNodeMap.get(hKey).setBackgroundColor(color_Red);
			}
		}

		final VerticalLayoutAlgorithm vl = new VerticalLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING);
		vl.setRowPadding(2);
		final TreeLayoutAlgorithm tl = new TreeLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING);
		graph.setLayoutAlgorithm(tl, true);
		graph.redraw();
		localParent.redraw();

	}

	private static String getKey(String key) {
		int idx = key.indexOf("(");
		if (idx != -1) {
			key = key.substring(0, idx);
		}
		idx = key.lastIndexOf(".");
		if (idx != -1) {
			key = key.substring(idx + 1);
		}

		key = key.replace(";->", "_");

		return key;
	}
}
