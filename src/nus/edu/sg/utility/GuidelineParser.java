package nus.edu.sg.utility;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import nus.edu.sg.ds.GuidelineData;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

// TODO: Auto-generated Javadoc
/**
 * The Class GuidelineParser.
 */
public class GuidelineParser {

	/** The xml data. */
	List<GuidelineData> xmlData;

	/**
	 * Parses the.
	 *
	 * @param filename
	 *            the filename
	 */
	public void parse(String filename) {

		xmlData = new ArrayList<GuidelineData>();

		try {

			final File fXmlFile = new File(filename);
			final DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			final DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			final Document doc = dBuilder.parse(fXmlFile);

			doc.getDocumentElement().normalize();

			// System.out.println("Root element :"
			// + doc.getDocumentElement().getNodeName());

			final NodeList nList = doc.getElementsByTagName("systemCall");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				final Node nNode = nList.item(temp);

				// System.out.println("\nCurrent Element :" +
				// nNode.getNodeName());
				final GuidelineData gData = new GuidelineData();

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					final Element eElement = (Element) nNode;

					/*
					 * System.out.println("SysCall Id : " +
					 * eElement.getAttribute("id"));
					 * System.out.println("SysCall description : " +
					 * eElement.getAttribute("name"));
					 * System.out.println("isPair : " +
					 * eElement.getElementsByTagName
					 * ("isPair").item(0).getTextContent());
					 * System.out.println("syscall first : " +
					 * eElement.getElementsByTagName
					 * ("syscall1").item(0).getTextContent());
					 * System.out.println("syscall second: " +
					 * eElement.getElementsByTagName
					 * ("syscall2").item(0).getTextContent());
					 * System.out.println("isRefCounted: " +
					 * eElement.getElementsByTagName
					 * ("isRefCounted").item(0).getTextContent());
					 * System.out.println("sysCallRef: " +
					 * eElement.getElementsByTagName
					 * ("sysCallRef").item(0).getTextContent());
					 */

					gData.id = Integer.parseInt(eElement.getAttribute("id"));
					gData.name = eElement.getAttribute("name");
					gData.type = eElement.getElementsByTagName("type").item(0).getTextContent();
					gData.isPair = Boolean.parseBoolean(eElement.getElementsByTagName("isPair").item(0)
							.getTextContent());
					gData.isUse = Boolean.parseBoolean(eElement.getElementsByTagName("isUse").item(0).getTextContent());
					gData.syscall1 = eElement.getElementsByTagName("syscall1").item(0).getTextContent();
					gData.syscall2 = eElement.getElementsByTagName("syscall2").item(0).getTextContent();
					gData.isRefCounted = Boolean.parseBoolean(eElement.getElementsByTagName("isRefCounted").item(0)
							.getTextContent());
					gData.sysCallRef = eElement.getElementsByTagName("sysCallRef").item(0).getTextContent();
					
					gData.setUseCalls(getUsagecallArray(eElement.getElementsByTagName("usage").item(0)));


					// if(gData.isUse){
					// System.out.println("use API "+gData.name);
					//
					// }else{
					// System.out.println("not use API");
					// }
				}

				final NodeList allChildNodes = nList.item(temp).getChildNodes();
				for (int i = 0; i < allChildNodes.getLength(); i++) {

					final Node childNode = allChildNodes.item(i);
					final String name = childNode.getNodeName();
					// System.out.println("none name "+name);
					if (name.equalsIgnoreCase("arguments")) {
						// System.out.println("arguments : ");
						gData.arguments = getAttributeNameArray(childNode);
					}

				}

				xmlData.add(gData);

			}

			// System.out.println("XML data parsing complete");

		} catch (final NullPointerException e) {
			System.out.println("\n\ninvalid XML file");
			e.printStackTrace();
		} catch (final SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (final ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Gets the xml data.
	 *
	 * @return the xml data
	 */
	public List<GuidelineData> getxmlData() {
		return xmlData;
	}

	/**
	 * Showxml data.
	 */
	public void showxmlData() {

		if (xmlData == null) {
			return;
		}

		for (int i = 0; i < xmlData.size(); i++) {
			final GuidelineData gData = xmlData.get(i);

			System.out.println("SysCall Id : " + gData.id);
			System.out.println("SysCall description : " + gData.name);
			System.out.println("isPair : " + gData.isPair);
			System.out.println("isUse : " + gData.isUse);
			System.out.println("syscall first : " + gData.syscall1);
			System.out.println("syscall second: " + gData.syscall2);
			System.out.println("isRefCounted: " + gData.isRefCounted);
			System.out.println("sysCallRef: " + gData.sysCallRef);
			System.out.println("argument size " + gData.arguments.size());

			for (int j = 0; j <= gData.arguments.size(); j++) {
				// System.out.println("\t "+gData.arguments.get(j));
			}
			// System.out.println();

		}
	}

	/**
	 * Gets the attribute name array.
	 *
	 * @param node
	 *            the node
	 * @return the attribute name array
	 */
	List<String> getAttributeNameArray(Node node) {
		// String name = node.getAttributes().item(0).getNodeValue();
		final NodeList allChildNodes = node.getChildNodes();
		final List<String> temp = new ArrayList<String>();

		for (int i = 0; i < allChildNodes.getLength(); i++) {

			final Node childNode = allChildNodes.item(i);
			if (childNode.getNodeName().equalsIgnoreCase("value")) {
				final String text = childNode.getTextContent();
				temp.add(text);
				// System.out.println("\t "+text);
			}
		}
		return temp;
	}
	
	
	
	List<String> getUsagecallArray(Node node) {
		// String name = node.getAttributes().item(0).getNodeValue();
		
		if (node == null) return null;
		
		final NodeList allChildNodes = node.getChildNodes();
		final List<String> temp = new ArrayList<String>();

		for (int i = 0; i < allChildNodes.getLength(); i++) {

			final Node childNode = allChildNodes.item(i);
			if (childNode.getNodeName().equalsIgnoreCase("single")) {
				final String text = childNode.getTextContent().replace('.', '/').replace("->", ";->");
				temp.add(text);
			}
		}
		return temp;
	}

	
}