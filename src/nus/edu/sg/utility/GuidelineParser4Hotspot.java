package nus.edu.sg.utility;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import nus.edu.sg.ds.GuidelineData4Hotspot;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

// TODO: Auto-generated Javadoc
/**
 * The Class GuidelineParser.
 */
public class GuidelineParser4Hotspot {

	/** The xml data. */
	private List<GuidelineData4Hotspot> xmlData;

	/**
	 * Parses the.
	 *
	 * @param filename
	 *            the filename
	 */
	public void parse(String filename) {

		xmlData = new ArrayList<GuidelineData4Hotspot>();

		try {

			final File fXmlFile = new File(filename);
			final DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			final DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			final Document doc = dBuilder.parse(fXmlFile);

			doc.getDocumentElement().normalize();

			// System.out.println("Root element :"
			// + doc.getDocumentElement().getNodeName());

			final NodeList nList = doc.getElementsByTagName("systemCall");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				final Node nNode = nList.item(temp);

				// System.out.println("\nCurrent Element :" +
				// nNode.getNodeName());
				final GuidelineData4Hotspot gData = new GuidelineData4Hotspot();

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					final Element eElement = (Element) nNode;

					/*
					 * System.out.println("SysCall Id : " +
					 * eElement.getAttribute("id"));
					 * System.out.println("SysCall description : " +
					 * eElement.getAttribute("name"));
					 * System.out.println("isPair : " +
					 * eElement.getElementsByTagName
					 * ("isPair").item(0).getTextContent());
					 * System.out.println("syscall first : " +
					 * eElement.getElementsByTagName
					 * ("syscall1").item(0).getTextContent());
					 * System.out.println("syscall second: " +
					 * eElement.getElementsByTagName
					 * ("syscall2").item(0).getTextContent());
					 * System.out.println("isRefCounted: " +
					 * eElement.getElementsByTagName
					 * ("isRefCounted").item(0).getTextContent());
					 * System.out.println("sysCallRef: " +
					 * eElement.getElementsByTagName
					 * ("sysCallRef").item(0).getTextContent());
					 */

					gData.setId(Integer.parseInt(eElement.getAttribute("id")));
					gData.setName(eElement.getAttribute("name"));
					gData.setType(eElement.getElementsByTagName("type").item(0).getTextContent());
					gData.setExclusive(Boolean.parseBoolean(eElement.getElementsByTagName("exclusive").item(0)
							.getTextContent()));
					gData.setBegincall(eElement.getElementsByTagName("begincall").item(0).getTextContent()
							.replace('.', '/').replace("->", ";->"));
					gData.setEndcall(eElement.getElementsByTagName("endcall").item(0).getTextContent()
							.replace('.', '/').replace("->", ";->"));
					gData.setUsagecalls(getUsagecallArray(eElement.getElementsByTagName("usage").item(0)));
					gData.setPairedUseCalls(getPairedUseCallTable(eElement.getElementsByTagName("usage").item(0)));
				}

				xmlData.add(gData);
			}

			// System.out.println("XML data parsing complete");

		} catch (final NullPointerException e) {
			System.out.println("\n\ninvalid XML file");
			e.printStackTrace();
		} catch (final SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (final ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Gets the xml data.
	 *
	 * @return the xml data
	 */
	public List<GuidelineData4Hotspot> getxmlData() {
		return xmlData;
	}

	/**
	 * Showxml data.
	 */
	public void showxmlData() {

		if (xmlData == null) {
			return;
		}

		for (int i = 0; i < xmlData.size(); i++) {
			final GuidelineData4Hotspot gData = xmlData.get(i);

			System.out.println("SysCall Id : " + gData.getId());
			System.out.println("SysCall Name : " + gData.getName());
			System.out.println("type : " + gData.getType());
			System.out.println("exclusive : " + gData.isExclusive());
			System.out.println("begin syscall : " + gData.getBegincall());

			for (int j = 0; j < gData.getUsagecalls().size(); j++) {
				System.out.printf("usage syscall : %s\n", gData.getUsagecalls().get(j));
			}

			System.out.println("end syscall : " + gData.getEndcall());
			System.out.println();

		}
	}

	/**
	 * Gets the attribute name array.
	 *
	 * @param node
	 *            the node
	 * @return the attribute name array
	 */
	List<String> getUsagecallArray(Node node) {
		// String name = node.getAttributes().item(0).getNodeValue();
		final NodeList allChildNodes = node.getChildNodes();
		final List<String> temp = new ArrayList<String>();

		for (int i = 0; i < allChildNodes.getLength(); i++) {

			final Node childNode = allChildNodes.item(i);
			if (childNode.getNodeName().equalsIgnoreCase("single")) {
				final String text = childNode.getTextContent().replace('.', '/').replace("->", ";->");
				temp.add(text);
			}
		}
		return temp;
	}

	/**
	 * Gets the attribute name array.
	 *
	 * @param node
	 *            the node
	 * @return the attribute name array
	 */
	Hashtable<String, String> getPairedUseCallTable(Node node) {
		// String name = node.getAttributes().item(0).getNodeValue();
		final NodeList allChildNodes = node.getChildNodes();
		final Hashtable<String, String> temp = new Hashtable<String, String>();

		for (int i = 0; i < allChildNodes.getLength(); i++) {

			final Node childNode = allChildNodes.item(i);
			if (childNode.getNodeName().equalsIgnoreCase("paired")) {

				final NodeList pairedChildNodes = childNode.getChildNodes();
				String startStr = "undefined";

				for (int j = 0; j < pairedChildNodes.getLength(); j++) {

					final Node pNode = pairedChildNodes.item(j);
					if (pNode.getNodeName().equalsIgnoreCase("startUse")) {
						startStr = pNode.getTextContent().replace('.', '/').replace("->", ";->");
					} else if (pNode.getNodeName().equalsIgnoreCase("stopUse")) {
						temp.put(startStr, pNode.getTextContent().replace('.', '/').replace("->", ";->"));
					}
				}
			}
		}
		return temp;
	}
}