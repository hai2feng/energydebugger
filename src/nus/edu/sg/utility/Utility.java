package nus.edu.sg.utility;

import java.util.List;

import nus.edu.sg.ds.DNode;

public class Utility {

	public static boolean inPattern(DNode node, String p) {

		final String[] ps = p.split(";->");
		if (ps.length > 1) {
			if (ps[0].contains(node.getClassName()) && ps[1].contains(node.getMethodName())) {
				return true;
			}
		} else {
			if (ps[0].contains(node.getClassName())) {
				return true;
			}
		}

		return false;
	}

	public static boolean inPattern(DNode node, List<String> patterns) {

		for (final String p : patterns) {
			final String[] ps = p.split(";->");
			if (ps.length > 1) {
				if (ps[0].contains(node.getClassName()) && ps[1].contains(node.getMethodName())) {
					return true;
				}
			} else {
				if (ps[0].contains(node.getClassName()) || node.getMethodName().contains(ps[0])) {
					return true;
				}
			}
		}

		return false;
	}

}
