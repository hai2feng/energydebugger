package nus.edu.sg;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import nus.edu.sg.ds.ClassNode;
import nus.edu.sg.ds.DNode;
import nus.edu.sg.ds.DTree;
import nus.edu.sg.ds.Defect;
import nus.edu.sg.ds.DefectInfo;
import nus.edu.sg.ds.Hotspot;

import org.eclipse.jdt.core.IMethod;

import plugin.utility.Parser;

public class Pipe {

	public static void propagate(DTree dTree) {

		if (Global.defectList == null) {
			return;
		}

		int i = 0;
		int count = 0;
		Object[] old;

		final Map<String, String> unique = new HashMap<String, String>();
		for (final DefectInfo d : Global.defectList) {
			for (final Defect dd : d.getDefect()) {
				if (!unique.containsKey(dd.toString()) && dd.subType != 3) {
					unique.put(dd.toString(), "blah");
					getRepairLocation(d, dd, dTree);
					count++;
				}
			}
		}

		if (!Global.debugListInit) {
			Global.debugList = new Object[count];
			Global.debugListInit = true;
		} else {
			old = Global.debugList;
			Global.debugList = new Object[count + old.length];
			for (final Object o : old) {
				Global.debugList[i++] = o;
			}

		}

		System.out.println("adding to debug list");
		unique.clear();
		for (final DefectInfo d : Global.defectList) {
			for (final Defect dd : d.getDefect()) {
				if (!unique.containsKey(dd.toString()) && dd.subType != 3) {
					dd.envn = d.getActivityEnv();
					Global.debugList[i++] = dd;
					System.out.println(dd);
					unique.put(dd.toString(), "blah");
				}
			}
		}

	}

	private static void getRepairLocation(DefectInfo d, Defect dd, DTree dTree) {

		if (dd.toString().contains("null")) {
			return;
		}

		final int idx = dd.getCaller().indexOf(";->");
		if (idx == 1) {
			return;
		}

		final String cls = dd.getCaller().substring(0, idx);
		if (!Global.sourceToLineMap.containsKey(cls)) {
			return;
		}
		final ClassNode rnode = Global.sourceToLineMap.get(cls);

		String subtype = " resource ";
		if (rnode.isService()) {
			subtype = " service ";
		}

		if (rnode.isListener()) {
			subtype = " listener ";
		}

		if (dd.type == Defect.BUG) {
			suggestRepairBug(dTree, dd, rnode, cls, subtype);
		} else {
			suggestRepairHotpsot(dTree, dd, rnode, cls, subtype);
		}

	}

	private static void suggestRepairHotpsot(DTree dTree, Defect dd, ClassNode rnode, String cls, String subtype) {
		if (dd.subType == 5) {

		} else if (dd.subType == 6) {

		} else if (dd.subType == 7) {
			System.out.println(cls + " uses resource with frequency " + ((Hotspot) dd).frequency
					+ "(used parameters : " + ((Hotspot) dd).params + ")");
			dd.patchString = cls + " uses resource with frequency " + ((Hotspot) dd).frequency + "(used parameters : "
					+ ((Hotspot) dd).params + ")";
		} else {

		}

		if (dTree == null) {
			return;
		}

		dd.before = dTree.getActivityBefore(dd.getTimeStamp());
		dd.after = dTree.getActivityAfter(dd.getTimeStamp());

	}

	private static void suggestRepairBug(DTree dTree, Defect dd, ClassNode rnode, String cls, String subtype) {

		if (dTree == null) {
			return;
		}

		dd.before = dTree.getActivityBefore(dd.getTimeStamp());
		dd.after = dTree.getActivityAfter(dd.getTimeStamp());

		if (!Global.putReleaseInLastActivity) { // release
			// resource/service/listener in
			// the acquiring activity
			if (rnode.isActivity()) {
				boolean resActonPauseCalled = false;
				final int lidx = cls.lastIndexOf(".");
				String resActivity = cls;
				if (lidx != -1) {
					resActivity = cls.substring(lidx + 1);
				}
				for (int i = 0; i < dd.after.size(); i++) {
					if (dd.after.get(i).getClassName().contains(resActivity)
							&& dd.after.get(i).getMethod().contains("onPause()V")) {
						resActonPauseCalled = true;
					}
				}

				if (!resActonPauseCalled) {
					System.out.println("Create method onPause() in activity : " + resActivity);
					dd.patchString = "Create method onPause() in activity : " + resActivity + "\n";
				} else {
					dd.patchString = "";
				}

				System.out.println("Release" + subtype + "in class " + cls + " in method onPause()of class "
						+ resActivity);
				dd.patchString += "Release" + subtype + "in class " + cls + " in method onPause()of class "
						+ resActivity;

			} else {

				final String resActivity = suggest_caller(dd);
				boolean resActonPauseCalled = false;

				for (int i = 0; i < dd.after.size(); i++) {
					if (dd.after.get(i).getClassName().contains(resActivity)
							&& dd.after.get(i).getMethod().contains("onPause()V")) {
						resActonPauseCalled = true;
					}
				}

				if (!resActonPauseCalled) {
					System.out.println("Create method onPause() in activity : " + resActivity);
					dd.patchString = "Create method onPause() in activity : " + resActivity + "\n";
				} else {
					dd.patchString = "";
				}

				System.out.println("Release" + subtype + "in class " + cls + " in method onPause() of class "
						+ resActivity);
				dd.patchString += "Release" + subtype + "in class " + cls + " in method onPause() of class "
						+ resActivity;

			}
		} else { // release resource in the last activity observed during the
			// testing
			// ClassNode lastActivity = null;
			// String lastNodeName = null;
			// boolean onPauseCalled = false;
			//
			// for (int i = dd.after.size() - 1; i >= 0; i--) {
			// final DNode x = dd.after.get(i);
			// final String clx =
			// Global.getWithoutDir(x.getFile()).replace("\\", ".").replace("/",
			// ".");
			// if (!Global.sourceToLineMap.containsKey(clx)) {
			// continue;
			// }
			// final ClassNode cnode = Global.sourceToLineMap.get(clx);
			// if (cnode.isActivity()) {
			// lastActivity = cnode;
			// lastNodeName = x.getClassName();
			// if (x.getMethod().contains("onPause()V")) {
			// onPauseCalled = true;
			// }
			// break;
			// }
			// }
			//
			// if (lastActivity == null) {
			// System.out.println("Can't determine last activity");
			// return;
			// }
			//
			// if (!onPauseCalled) {
			// System.out.println("Create method onPause() in activity : " +
			// lastActivity);
			// dd.patchString = "Create method onPause() in activity : " +
			// lastActivity + "\n";
			// } else {
			// dd.patchString = "";
			// }
			//
			// if (rnode.isService()) {
			// System.out.println("Release service " + rnode.getFileName() +
			// " in class " + lastActivity.getFileName()
			// + " in method onPause()");
			// dd.patchString += "Release service " + rnode.getFileName() +
			// " in class " + lastActivity.getFileName()
			// + " in method onPause()\n";
			// } else {
			// System.out.println("Release resource in class " +
			// lastActivity.getFileName() + " in method onPause()");
			// dd.patchString += "Release resource in class " +
			// lastActivity.getFileName() + " in method onPause()";
			// }
			// System.out.println("===============================");
		} // last activity release finished

	}

	private static String suggest_caller(Defect dd) {

		String key = dd.getCaller();
		final int ide = key.indexOf("(");
		if (ide != -1) {
			key = key.substring(0, ide);
		}

		System.out.println("After : " + dd.after);
		System.out.println("Before : " + dd.before);
		final Map<String, Boolean> potentialCallers = new HashMap<String, Boolean>();
		if (Global.iMethodMap.containsKey(key)) {
			final HashSet<IMethod> callers = getCaller(key);

			for (final IMethod c : callers) {
				String getClassName = c.getPath().toString();
				// System.out.println(c.getParent().getElementName() + ": " +
				// c.getPath().toString());
				getClassName = getClassName.replace("/", ".").replace(".java", "");
				final int ids = getClassName.indexOf(".src.");
				if (ids != -1) {
					getClassName = getClassName.substring(ids + 5);
				}
				if (Global.sourceToLineMap.containsKey(getClassName)) {
					final ClassNode node = Global.sourceToLineMap.get(getClassName);
					if (node.isActivity()) {
						potentialCallers.put(node.getName().replace(".java", ""), true);
					} else {
						potentialCallers.put(node.getName().replace(".java", ""), false);
					}
				}

			}
		}

		for (int i = dd.before.size() - 1; i >= 0; i--) {
			final DNode n = dd.before.get(i);
			if (potentialCallers.containsKey(n.getClassName())) {
				if (potentialCallers.get(n.getClassName())) {
					return n.getClassName();
				}
			}
		}
		System.out.println("Potential callers : " + potentialCallers);

		return "Either[" + potentialCallers + "]";

	}

	private static HashSet<IMethod> getCaller(String key) {
		HashSet<IMethod> cl = Parser.getCallersOf(Global.iMethodMap.get(key));
		String orgClass = key;
		final int idx = key.indexOf(";->");
		if (idx != -1) {
			orgClass = key.substring(0, idx);
		}
		if (cl.size() == 1) {
			for (final IMethod c : cl) {
				String getClassName = c.getPath().toString();
				getClassName = getClassName.replace("/", ".").replace(".java", "");
				final int ids = getClassName.indexOf(".src.");
				getClassName = getClassName.substring(ids + 5);

				if (getClassName.equals(orgClass)) {// this may need to be done
					// recursively
					key = getClassName + ";->" + c.getElementName();
					cl = Parser.getCallersOf(Global.iMethodMap.get(key));
				}
			}

		}

		return cl;
	}
}
