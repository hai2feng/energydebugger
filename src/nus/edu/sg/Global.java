package nus.edu.sg;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nus.edu.sg.ds.ClassNode;
import nus.edu.sg.ds.DefectInfo;
import nus.edu.sg.ds.EnergyBug;

import org.eclipse.core.resources.IFile;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.ui.console.MessageConsoleStream;

public class Global {
	// public static String xmlFilePath =
	// "C:/Users/workshop/git/energydebugger/input/guideline.xml";

	public static String xmlFilePath = "C:/Users/dcsghf/workspace/EnergyBugDetector/input/guideline.xml";

	// public static String logPath =
	// "C:/Users/dcsghf/Desktop/app_data/patched/sofia_issue76/logSPTN76.txt";
	public static String logPath = "C:/Users/dcsghf/LoggerTool/logfile_sptn38.txt";

	// public static String outputFileName = "bugReportAripuca.txt";
	public static String outputFileName = "bugReportDAR.txt";

	/*** DO NOT EDIT BELOW THIS LINE ***/
	public static MessageConsoleStream out = null;
	public static String consoleName = "EnergyDebugger";
	public static boolean initPresent;
	public static String selectedProject;
	public static HashMap<String, ClassNode> sourceToLineMap;

	public static Object variablesNamesFromSource;
	public static boolean isXMLParsed;
	public static Object keyValue;
	public static Object listUIElement;
	public static Map<String, String> pathToProjects;
	public static int verbosity = 0;
	public static boolean parsingComplete = false;
	public static String plugindirectory;
	public static String rootLocation;
	public static boolean debugListInit = false;
	public static Object[] debugList;
	public static Object selectedDebug;
	public static IFile TEST_IFILE = null;
	public static List<EnergyBug> bugList;
	public static List<DefectInfo> defectList;
	// public static String instrumentationDir =
	// "dcsghf.workspace.EFGGen.apps_instrumented.extracted.";//
	public static String instrumentationDir = "extracted";
	public static String pathToSelectedProject;

	public static String hotspotXmlFilePath;

	public static boolean putReleaseInLastActivity = false;

	public static Map<String, IMethod> iMethodMap;

	public static float configurableRuntine = 0;

	public static String dotPath = "treevisual.dot";

	public static String getWithoutDir(String temp) {

		final int idx = temp.indexOf(instrumentationDir);
		if (idx != -1) {
			return temp.substring(idx + instrumentationDir.length() + 1);
		}

		return temp;
	}

}
